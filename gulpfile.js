var gulp = require('gulp');
var include = require('gulp-include');
var watch = require('gulp-watch');
var mjml = require('gulp-mjml');
var i18n = require('gulp-html-i18n');
var mjmlEngine = require('mjml');

var dest = './html';
var mjmlFiles = './mjml/templates/**/*.mjml';
var i18nOptions = {
      langDir: './lang',
      createLangDirs: true
    };

gulp.task('build', function () {
  return gulp.src(mjmlFiles)
    .pipe(include())
    .pipe(i18n(i18nOptions))
    .pipe(mjml(mjmlEngine))
    .pipe(gulp.dest(dest))
});

gulp.task('watch', function () {
  return watch(mjmlFiles)
    .pipe(include())
    .pipe(i18n(i18nOptions))
    .pipe(mjml(mjmlEngine))
    .pipe(gulp.dest(dest))
});

gulp.task('default', ['watch']);
